## Task
- Create 2 web servers that are served from a single public IP address behind load balancer called haproxy 
- At the end of the task you should have 3 instances running with only the load balancer accepting requests from the world and from anyone on port 80.  
- The other 2 servers will only allow port 80 from the load balancer on the private network, but all 3 will allow ssh from your IP address.

---

## Creating 3 instances 

### AWS Linux
- Click Launch Instance.
- Choose Amazon Linux 2 AMI (HVM), SSD Volume Type -ami-0ffea00000f287d30 (64-bit x86) / ami-00552336fb4b81164 (64-bit Arm).
- Choose t2.micro as the instance type.
- Keep instance details on default and move to next.
- Keep storage at 8 GiB
- Add the tags Name and Project, assigning mohamed-aws and assessment1 to them respectively. 
- Select existing security group. Select AcademySG and sc-mohamed-allow-access.
- Launch and select the key with your name on it.

### Ubuntu 
- Click Launch Instance.
- Ubuntu Server 18.04 LTS (HVM), SSD Volume Type - ami-06fd78dc2f0b69910 (64-bit x86) / ami-0fb9a10421d941ab2 (64-bit Arm)
- Choose t2.micro as the instance type.
- Keep instance details on default and move to next.
- Keep storage at 8 GiB
- Add the tags Name and Project, assigning ubuntu-aws and assessment1 to them respectively. 
- Select existing security group. Select AcademySG and sc-mohamed-allow-access.
- Launch and select the key with your name on it.

### lb(haproxy)
- Click Launch Instance.
- Choose Amazon Linux 2 AMI (HVM), SSD Volume Type -ami-0ffea00000f287d30 (64-bit x86) / ami-00552336fb4b81164 (64-bit Arm).
- Choose t2.micro as the instance type.
- Keep instance details on default and move to next.
- Keep storage at 8 GiB
- Add the tags Name and Project, assigning mohamed-lb and assessment1 to them respectively. 
- Select existing security group. Select AcademySG and sc-mohamed-allow-access.
- Launch and select the key with your name on it.


#!/bin/bash 

if (( $# > 0)) # Evaluates a condition (returns true/false/binary)
then
    hostname=$1 # $ is input argument in command line 
else 
    echo "WTF: you must supply a hostname or IP address" 1>&2 # Move and merge standard output to standard error. 
    exit 1 # Identify it as a failure 
fi

# -i identity key, -o options, we are using stricthostkeychecking to stop it from asking for fingerprint, 
ssh -o StrictHostKeyChecking=no -i ~/.ssh/MohamedNasrKey.pem ec2-user@$hostname  '

sudo yum -y install httpd # Installing Apache 

if rpm -qa | grep '^httpd-[0-9]' >/dev/null 2>&1
then
  sudo systemctl start httpd
else 
    exit 1 
fi

sudo useradd - instructor # Add user called instructor 
(sleep 2; echo m4g1cAut0L0t10n; sleep 2; echo m4g1cAut0L0t10n) | passwd instructor # Add password for this user 

sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1>Hello there</h1>
<h2>My Private IP address is </h2>
<h2>Mohamed</h2>
<h2>NAME=Amazon Linux, VERSION=2</h2>
<h2>Apache 2.4.46</h2>
_END_"
'
